let timePlay
let timeFeed
let timeSleep
// Premiere Bar

let feedOri = 100;
let elemFOri = document.getElementById('myBarFeed')

function decreaseFeed() {
    feedOri --;
    elemFOri.style.width = feedOri + "%";
}

setInterval(() => {
    if (feedOri <= 0){
        deadOri()
    }
    decreaseFeed()
}, 1000);

function increaseFeed() {
    if (feedOri < 100) {
    feedOri++;
    elemFOri.style.width = feedOri + "%";
    clearTimeout(timeFeed)
    if(document.getElementById('theImage').src !== "https://steamuserimages-a.akamaihd.net/ugc/272840175094808936/6E8A3B8E17E8BA3115707FD6E8966D02074CB72C/")
    document.getElementById('theImage').src="https://steamuserimages-a.akamaihd.net/ugc/272840175094808936/6E8A3B8E17E8BA3115707FD6E8966D02074CB72C/";
    timeFeed = setTimeout(() => {
        document.getElementById('theImage').src ="https://cdn.dribbble.com/users/1208648/screenshots/10752089/media/a22c8781e5517afc8f48691f88e22c32.gif"
    }, 4500);
    } 
}

    // Seconde Bar

    let sleepOri = 100;
    let elemSOri = document.getElementById('myBarSleep')
    
    function decreaseSleep() {
        sleepOri --;
        elemSOri.style.width = sleepOri + "%";
    }
    
    setInterval(() => {
        if (sleepOri <= 0){
            deadOri()
        }
        decreaseSleep()
    }, 1000);
    
    function increaseSleep() {
        if (sleepOri < 100) {
        sleepOri++;
        elemSOri.style.width = sleepOri + "%";
        clearTimeout(timeSleep);
        if(document.getElementById('theImage').src !== "https://steamuserimages-a.akamaihd.net/ugc/259337077221873976/462477FD1512682A93ED9366D561464CA9FB561E/")
        document.getElementById('theImage').src="https://steamuserimages-a.akamaihd.net/ugc/259337077221873976/462477FD1512682A93ED9366D561464CA9FB561E/";
        timeSleep = setTimeout(() => {
            document.getElementById('theImage').src ="https://cdn.dribbble.com/users/1208648/screenshots/10752089/media/a22c8781e5517afc8f48691f88e22c32.gif"
        }, 5000);
        }
    }

        //Troisieme Bar

        let playOri = 100;
    let elemPOri = document.getElementById('myBarPlay')
    
    function decreasePlay() {
        playOri --;
        elemPOri.style.width = playOri + "%";
    }
    
    setInterval(() => {
        if (playOri <= 0){
            deadOri()
        }
        decreasePlay()
    }, 1000);
    
    function increasePlay() {
        if (playOri < 100) {
        playOri++;
        elemPOri.style.width = playOri + "%";
        clearTimeout(timePlay);
        if(document.getElementById('theImage').src !== "https://thumbs.gfycat.com/BruisedPitifulArabianhorse-size_restricted.gif")
            document.getElementById('theImage').src="https://thumbs.gfycat.com/BruisedPitifulArabianhorse-size_restricted.gif";
            timePlay = setTimeout(() => {
                document.getElementById('theImage').src ="https://cdn.dribbble.com/users/1208648/screenshots/10752089/media/a22c8781e5517afc8f48691f88e22c32.gif"
            }, 5000);
        }
    }
function deadOri() {
    alert('Game Over')
}
window.addEventListener('keydown',
    function (event) {
        if (event.keyCode === 13){
            const area = document.getElementById('area')
            const listen = document.getElementById('inputOri')
            area.innerText = area.innerText + listen.value;
            if(listen.value === 'feed') {
                increaseFeed();
            } else if(listen.value === 'play') {
                increasePlay();
            } else if (listen.value === 'sleep') {
                increaseSleep();
            } else {
                alert('Commande indisponible')
            }
        }
});
